#[macro_use]
extern crate serde_derive;

pub mod serde_items;

use hyper::body::HttpBody as _;
use hyper::{Body, Request, Response};

use bytes::Bytes;

pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

#[derive(Copy, Clone, Debug)]
pub enum Error {
    Status(hyper::StatusCode)
}
impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use Error::*;
        write!(f, "{}", match self {
            Status(status) => status,
        })
    }
}
impl std::error::Error for Error {}

macro_rules! recv_body_fn {
    ($fname:ident, $src:tt) => {
        pub async fn $fname<F>(src: &mut $src<Body>, mut handler: F) -> Result<()>
        where F: FnMut(/*chunk:*/Bytes) -> Result<()>
        {
            #[cfg(feature = "trace")]
            print!(concat!("trace: ",stringify!($src)," Body {{ "));
            let res = recv_body(src.body_mut(), |bytes| {
                #[cfg(feature = "trace")]
                print!("{:?}", bytes);
                handler(bytes)
            }).await;
            #[cfg(feature = "trace")]
            println!(" }}");
            res
        }  
    }
}
recv_body_fn!(recv_req_body, Request);
recv_body_fn!(recv_resp_body, Response);

pub async fn recv_body<F>(body: &mut Body, mut handler: F) -> Result<()>
where F: FnMut(/*chunk:*/Bytes) -> Result<()>
{
    let mut res = Ok(());
    while let Some(chunk) = body.data().await {
        let bytes: Bytes = chunk?;
        res = handler(bytes);
        if res.is_err() {
            break;
        }
    }
    res
}