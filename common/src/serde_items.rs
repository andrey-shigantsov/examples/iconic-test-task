use std::collections::HashSet;
use std::hash::{Hash,Hasher};

#[derive(Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Message {
    pub text: String,
    pub key: String,
}
pub type MessageList = HashSet<Message>;

#[derive(Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Topic {
    pub name: String,
}
pub type TopicList = HashSet<Topic>;

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct TopicMessages {
    pub topic: String,
    pub messages: MessageList,
}
impl Hash for TopicMessages {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.topic.hash(state)
    }
}
pub type TopicMessagesList = HashSet<TopicMessages>;