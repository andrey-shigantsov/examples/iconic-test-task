# Messages broker - ICONIC Test Task

Simple in-memory messages broker

## Server usage

Build and run server

```shell
cargo run --manifest-path ./server/Cargo.toml
```
## Test client usage

Build and run test client

```shell
cargo run --manifest-path ./client/Cargo.toml
```

Read stdout for info

## Design

![general](doc/design.png)

### In-memory DB

![general](doc/in-memory-db.png)