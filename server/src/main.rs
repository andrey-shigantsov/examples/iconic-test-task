use iconic_broker::listen;

#[tokio::main]
pub async fn main() {
    // TODO: get addr from cmd arg
    let addr = "127.0.0.1:7777";

    listen(addr).await;
}
