mod db;

use std::net::ToSocketAddrs;
use std::convert::Infallible;

use tokio::time::{sleep, Duration};

use hyper::body::HttpBody as _;
use hyper::{Body, Request, Response, Method, StatusCode, Server};
use hyper::service::{make_service_fn, service_fn};
use hyper::server::conn::AddrStream;

use bytes::{BytesMut, BufMut as _};

use db::*;

macro_rules! resp_err {
    ($code:tt) => {
        resp_err!($code,"");
    };
    
    ($code:tt, $($body_arg:tt)+) => {{
        let body = format!($($body_arg)+);
        Response::builder()
        .status(StatusCode::$code)
        .body(body.into())
        .expect(stringify!("build Responce '", $code, "' failure: ", body))
    }};
}

pub async fn listen<A: ToSocketAddrs>(addr: A) {
    let mut addrs = match addr.to_socket_addrs() {
        Ok(addrs) => addrs,
        Err(e) => {
            eprintln!("failure: {}", e);
            return;
        },
    };

    let db = Db::new();

    let addr = if let Some(addr) = addrs.next() {
        addr
    } else {
        eprintln!("internal failure: get addr");
        return;
    };

    println!("listening on {}", addr);

    let make_svc = make_service_fn(|socket: &AddrStream| {
        let _remote_addr = socket.remote_addr();
        let db = db.clone();
        async move {
            Ok::<_, Infallible>(service_fn(move |req| {
                let db = db.clone();
                async move { service(db, req).await }
            }))
        }
    });

    let server = Server::bind(&addr).serve(make_svc);
        
    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }
}

async fn service(db: Db, mut req: Request<Body>) -> Result<Response<Body>,Infallible> {
    #[cfg(feature = "trace")]
    println!("trace: {:?}", req);

    let resp = match (req.method(), req.uri().path()) {
        (&Method::GET, "/") => { // for test
            Response::new("Hello, World".into())
        },
        (&Method::GET, "/sleep") => { // for test
            sleep(Duration::from_millis(15000)).await;
            Response::new("This medicine makes me drowsy".into())
        },
        (&Method::GET, "/topics") => { // for test
            get_topic_list(&db)
        },
        (&Method::PUT, "/topic/create") => {
            let mut name = String::new();
            if let Err(err_resp) = parse_uri(req.uri(), |query| {
                parse_query(query, |key, val| {
                    match key {
                        "name" => name = String::from(val),
                        _ => {}
                    }
                });
            }) {
                err_resp
            } else {
                create_topic(&db, &name)
            }
        },
        (&Method::PUT, "/topic/subscribe") => {
            let mut name = String::new();
            let mut user = String::new();
            if let Err(err_resp) = parse_uri(req.uri(), |query| {
                parse_query(query, |key, val| {
                    match key {
                        "name" => name = String::from(val),
                        "user" => user = String::from(val),
                        _ => {}
                    }
                });
            }) {
                err_resp
            } else {
                subscribe(&db, &name, &user)
            }
        },
        (&Method::PUT, "/topic/unsubscribe") => {
            let mut name = String::new();
            let mut user = String::new();
            if let Err(err_resp) = parse_uri(req.uri(), |query| {
                parse_query(query, |key, val| {
                    match key {
                        "name" => name = String::from(val),
                        "user" => user = String::from(val),
                        _ => {}
                    }
                });
            }) {
                err_resp
            } else {
                unsubscribe(&db, &name, &user)
            }
        },
        (&Method::POST, "/message") => {
            let mut topic = String::new();
            if let Err(err_resp) = parse_uri(req.uri(), |query| {
                parse_query(query, |key, val| {
                    match key {
                        "topic" => topic = String::from(val),
                        _ => {}
                    }
                });
            }) {
                err_resp
            } else {
                // TODO: remove and implement streaming parsing of json // NOTE: need learn serde
                let mut body = {
                    if let Some(len) = req.body().size_hint().exact()/*req.headers().get("content-length")*/ {
                        #[cfg(feature = "trace")]
                        println!("trace: Request Body Len = {}", len);
                        BytesMut::with_capacity(len as usize)
                    } else {
                        BytesMut::new()
                    }
                };
                if let Err(e) = common::recv_req_body(&mut req, |chunk| {
                    body.put(chunk);
                    Ok(())
                }).await {
                    eprintln!("Request body receive failure: {}", e);
                    resp_err!(BAD_REQUEST, "Request body receive failure: {}", e)
                } else {
                    let msg: Result<common::serde_items::Message,_> = serde_json::from_slice(&body);
                    match msg {
                        Err(e) => {
                            #[cfg(feature = "trace")]
                            println!("trace: Add message: json parse failure: {}", e);
                            resp_err!(BAD_REQUEST, "Request body parse failure: {}", e)
                        },
                        Ok(msg) => {
                            add_message(&db, &topic, &msg.text, &msg.key)
                        }
                    }
                }
            }
        },
        (&Method::GET, "/messages") => {
            let mut user = String::new();
            if let Err(err_resp) = parse_uri(req.uri(), |query| {
                parse_query(query, |key, val| {
                    match key {
                        "user" => user = String::from(val),
                        _ => {}
                    }
                });
            }) {
                err_resp
            } else {
                get_message_list(&db, &user)
            }
        },
        (_, _) => resp_err!(NOT_FOUND)
    };

    #[cfg(feature = "trace")]
    println!("trace: {:?}", resp);

    Ok(resp)
}

fn get_topic_list(db: &Db) -> Response<Body> {
    #[cfg(feature = "trace")]
    println!("trace: Get topic list");

    let json_list = {
        use common::serde_items::{Topic, TopicList};
        let topics = db.topics.lock().unwrap();
        let list: TopicList = {
            topics.keys().map(|name| 
                Topic { name: name.clone() }
            ).collect()
        };
        drop(topics);
        serde_json::to_string(&list)
    };
    match json_list {
        Err(e) => {
            eprintln!("Get topic list: json gen failure: {}", e);
            resp_err!(INTERNAL_SERVER_ERROR, "Gen list failure")
        },
        Ok(json_list) => {
            match Response::builder()
                .header("content-type", "application/json")
                .body(json_list.into())
            {
                Ok(resp) => resp,
                Err(e) => {
                    eprintln!("Get topic list: build response failure: {}", e);
                    resp_err!(INTERNAL_SERVER_ERROR)
                }
            }

        }
    }
}

fn create_topic(db: &Db, name: &str) -> Response<Body> {
    if name.is_empty() {
        return resp_err!(BAD_REQUEST, "Topic name empty")
    }

    #[cfg(feature = "trace")]
    println!("trace: Create topic \"{}\"", name);

    let mut topics = db.topics.lock().unwrap(); 
    if topics.contains_key(name) {
        return resp_err!(BAD_REQUEST, "Topic \"{}\" exists", name);
    }       
    topics.insert(String::from(name), Topic::new());
    drop(topics);
    
    let s = format!("Topic \"{}\" crated", name);
    println!("{}", s);
    Response::new(s.into())
}

fn subscribe(db: &Db, topic: &str, user: &str) -> Response<Body> {
    if topic.is_empty() {
        return resp_err!(BAD_REQUEST, "Topic name empty")
    }
    if user.is_empty() {
        return resp_err!(BAD_REQUEST, "User name empty")
    }

    #[cfg(feature = "trace")]
    println!("trace: Subscribe user \"{}\" to topic \"{}\"", user, topic);

    let topics = db.topics.lock().unwrap(); 
    if !topics.contains_key(topic) {
        return resp_err!(BAD_REQUEST, "Topic \"{}\" not exists", topic);
    }
    drop(topics);

    let mut subscribes = db.subscribes.lock().unwrap();
    if !subscribes.contains_key(user) {
        subscribes.insert(String::from(user), TopicNameList::new());
    }
    let topics = subscribes.get_mut(user).unwrap();
    topics.insert(String::from(topic));
    drop(subscribes);

    println!("User \"{}\" subscribed to topic \"{}\"", user, topic);
    Response::new("Success".into())
}

fn unsubscribe(db: &Db, topic: &str, user: &str) -> Response<Body> {
    if topic.is_empty() {
        return resp_err!(BAD_REQUEST, "Topic name empty")
    }
    if user.is_empty() {
        return resp_err!(BAD_REQUEST, "User name empty")
    }

    #[cfg(feature = "trace")]
    println!("trace: Unsubscribe user \"{}\" from topic \"{}\"", user, topic);

    let topics = db.topics.lock().unwrap(); 
    if !topics.contains_key(topic) {
        return resp_err!(BAD_REQUEST, "Topic \"{}\" not exists", topic);
    }
    drop(topics);

    let mut subscribes = db.subscribes.lock().unwrap();
    if !subscribes.contains_key(user) {
        return resp_err!(BAD_REQUEST, "User \"{}\" not registred", user);
    }
    let topics = subscribes.get_mut(user).unwrap();
    if !topics.contains(topic) {
        return resp_err!(BAD_REQUEST, "User \"{}\" not subscribed for topic \"{}\"", user, topic);
    }
    topics.remove(topic);
    drop(subscribes);

    println!("User \"{}\" unsubscribed from topic \"{}\"", user, topic);
    Response::new("Success".into())
}

fn add_message(db: &Db, topic_name: &str, text: &str, key: &str) -> Response<Body> {
    if topic_name.is_empty() {
        return resp_err!(BAD_REQUEST, "Topic name empty")
    }
    if text.is_empty() {
        return resp_err!(BAD_REQUEST, "Message text empty")
    }
    
    let msg = Message::new(text);

    #[cfg(feature = "trace")]
    println!("trace: Add {:?} to topic \"{}\"", msg, topic_name);

    let mut topics = db.topics.lock().unwrap();
    match topics.get_mut(topic_name) {
        Some(topic) => {
            topic.add_message(String::from(key), msg);
            drop(topics);

            println!("New message in topic \"{}\"", topic_name);
            Response::new(format!("Message added to topic \"{}\"", topic_name).into())
        },
        None => {
            drop(topics);
            resp_err!(BAD_REQUEST, "Invalid topic")
        }
    }
}

fn get_message_list(db: &Db, user: &str) -> Response<Body> {
    if user.is_empty() {
        return resp_err!(BAD_REQUEST, "User name empty")
    }

    #[cfg(feature = "trace")]
    println!("trace: Get message list for user \"{}\"", user);

    let mut subscribes = db.subscribes.lock().unwrap();
    if !subscribes.contains_key(user) {
        return resp_err!(BAD_REQUEST, "User \"{}\" not registred", user);
    }

    let json_list = {
        use common::serde_items::{Message, MessageList, TopicMessages, TopicMessagesList};
        let mut list = TopicMessagesList::new();

        let db_topic_names = subscribes.get_mut(user).unwrap();
        for topic_name in db_topic_names.iter() {
            let db_topics = db.topics.lock().unwrap(); 
            match db_topics.get(topic_name) {
                Some(topic) => {
                    list.insert(TopicMessages {
                        topic: topic_name.clone(),
                        messages: {
                            let mut msg_list = MessageList::new();
                            topic.messages_handler(|key, msg| {
                                msg_list.insert(Message {
                                    key: key.clone(),
                                    text: msg.text.clone(),
                                });
                            });
                            msg_list
                        },
                    });
                },
                None => {
                    eprintln!("Get message list: topic {} not exists", topic_name);
                }
            }        
            drop(db_topics);
        }
        drop(subscribes);
        serde_json::to_string(&list)
    };
    match json_list {
        Err(e) => {
            eprintln!("Get message list: json gen failure: {}", e);
            resp_err!(INTERNAL_SERVER_ERROR, "Gen list failure")
        },
        Ok(json_list) => {
            match Response::builder()
                .header("content-type", "application/json")
                .body(json_list.into())
            {
                Ok(resp) => resp,
                Err(e) => {
                    eprintln!("Get topic list: build response failure: {}", e);
                    resp_err!(INTERNAL_SERVER_ERROR)
                }
            }
        }
    }
}

fn parse_uri<F>(uri: &hyper::Uri, mut handler: F) -> Result<(),Response<Body>>
where F: FnMut(/*query:*/&str) {
    if let Some(query) = uri.query() {
        handler(query);
        Ok(())
    } else {
        Err(resp_err!(BAD_REQUEST, "Empty query"))
    }

}
fn parse_query<F>(query: &str, mut handler: F)
where F: FnMut(/*key:*/&str, /*val:*/&str)
{
    let params = url::form_urlencoded::parse(query.as_bytes());

    for (key, val) in params {
        #[cfg(feature = "trace")]
        println!("trace: Parse query: \"{}\" = \"{}\"", key, val);

        handler(&*key, &*val);
    }
}
