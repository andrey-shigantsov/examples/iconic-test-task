use std::collections::{HashMap,HashSet};
use std::sync::{Arc, Mutex};

#[derive(Clone)]
pub struct Db { // TODO: not use mutex (see https://tokio.rs/tokio/tutorial/shared-state#tasks-threads-and-contention)
    pub topics: Arc<Mutex<TopicMap>>,
    pub subscribes: Arc<Mutex<SubscribeMap>>,
}
impl Db {
    pub fn new() -> Self {
        Self {
            topics: Arc::new(Mutex::new(TopicMap::new())),
            subscribes: Arc::new(Mutex::new(SubscribeMap::new())),
        }
    }
}

pub type TopicMap = HashMap</*name:*/String, Topic>;
pub type TopicNameList = HashSet</*name:*/String>;

#[derive(Debug)]
pub struct Topic {
    messages_with_key: MessageMap,
    messages_without_key: MessageList,
}
impl Topic {
    pub fn new() -> Self {
        Self {
            messages_with_key: MessageMap::new(),
            messages_without_key: MessageList::new(),
        }
    }

    pub fn add_message(&mut self, key: MessageKey, msg: Message) {
        if key.is_empty() {
            self.messages_without_key.insert(msg);
        } else {
            self.messages_with_key.insert(key, msg);
        }
    }

    pub fn messages_handler<F>(&self, mut handler: F)
    where F: FnMut(&MessageKey, &Message)
    {
        for (key, msg) in self.messages_with_key.iter() {
            handler(key, msg);
        }
        let empty_key = MessageKey::from("");
        for msg in self.messages_without_key.iter() {
            handler(&empty_key, msg);
        }

    }
}

pub type MessageKey = String;
pub type MessageList = HashSet<Message>;
pub type MessageMap = HashMap<MessageKey, Message>;

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Message {
    pub text: String,
}

impl Message {
    pub fn new(msg: &str) -> Self {
        Self {
            text: String::from(msg),
        }
    }
}

pub type SubscribeMap = HashMap</*user:*/String,/*topics:*/TopicNameList>;