use hyper::{Body, Method, Request};

use bytes::{BytesMut, BufMut as _};

pub struct Client {
    inner: hyper::Client<hyper::client::connect::HttpConnector>,
    host: String,
    user: String,
}

fn percent_encode(s: &str) -> String {
    use percent_encoding::{utf8_percent_encode, NON_ALPHANUMERIC};
    utf8_percent_encode(s, NON_ALPHANUMERIC).to_string()
}

macro_rules! uri {
    ($client:expr $(,$($arg:tt)+)?) => {{
        format!(
            "http://{}{}",
            &$client.host $(,format_args!($($arg)+))?
        )
    }};
}

// TODO: rafactor: create and use macro for build and do requestes
impl Client {
    pub fn new(host: &str, user: &str) -> Self {
        Self {
            inner: hyper::Client::new(),
            host: String::from(host),
            user: String::from(user),
        }
    }

    pub async fn topics(&mut self) -> common::Result<()> {
        let uri = uri!(self, "/topics");
        #[cfg(feature = "trace")]
        println!("trace: Topic list: {}", uri);
        println!("Get topic list request sending...");

        let req = Request::builder()
            .method(Method::GET)
            .uri(uri)
            .body("".into())?;

        #[cfg(feature = "trace")]
        println!("trace: {:?}", req);

        let mut resp = self.inner.request(req).await?;

        #[cfg(feature = "trace")]
        println!("trace: {:?}", resp);

        let mut body = BytesMut::new();
        common::recv_resp_body(&mut resp, |chunk| {
            body.put(chunk);
            Ok(())
        }).await?;

        let status = resp.status();
        if !status.is_success() {
            println!("Get topic list failure: server says: \"{}\": {:?}", status, body);
            return Err(Box::new(common::Error::Status(status)));
        }

        println!("Topics {:?}", body);
        Ok(())
    }

    pub async fn subscribe(&mut self, topic: &str) -> common::Result<()> {
        let uri = uri!(self, "/topic/subscribe?name={}&user={}",
            percent_encode(topic), percent_encode(&self.user)
        );
        #[cfg(feature = "trace")]
        println!("trace: Subsribe: {}", uri);
        println!("Subscribe \"{}\" request sending...", topic);

        let req = Request::builder()
            .method(Method::PUT)
            .uri(uri)
            .body("".into())?;

        #[cfg(feature = "trace")]
        println!("trace: {:?}", req);

        let mut resp = self.inner.request(req).await?;

        #[cfg(feature = "trace")]
        println!("trace: {:?}", resp);

        let mut body = BytesMut::new();
        common::recv_resp_body(&mut resp, |chunk| {
            body.put(chunk);
            Ok(())
        }).await?;

        let status = resp.status();
        if !status.is_success() {
            println!("Subscribe failure: server says: \"{}\": {:?}", status, body);
            return Err(Box::new(common::Error::Status(status)));
        }

        println!("Subscribed to topic \"{}\"", topic);
        Ok(())
    }

    pub async fn unsubscribe(&mut self, topic: &str) -> common::Result<()> {
        let uri = uri!(self, "/topic/unsubscribe?name={}&user={}",
            percent_encode(topic), percent_encode(&self.user)
        );
        #[cfg(feature = "trace")]
        println!("trace: Unubsribe: {}", uri);
        println!("Unsubscribe \"{}\" request sending...", topic);

        let req = Request::builder()
            .method(Method::PUT)
            .uri(uri)
            .body("".into())?;

        #[cfg(feature = "trace")]
        println!("trace: {:?}", req);

        let mut resp = self.inner.request(req).await?;

        #[cfg(feature = "trace")]
        println!("trace: {:?}", resp);

        let mut body = BytesMut::new();
        common::recv_resp_body(&mut resp, |chunk| {
            body.put(chunk);
            Ok(())
        }).await?;

        let status = resp.status();
        if !status.is_success() {
            println!("Unsubscribe failure: server says: \"{}\": {:?}", status, body);
            return Err(Box::new(common::Error::Status(status)));
        }

        println!("Unsubscribed from topic \"{}\"", topic);
        Ok(())
    }

    pub async fn create_topic(&mut self, name: &str) -> common::Result<()> {
        let uri = uri!(self, "/topic/create?name={}", percent_encode(name));
        #[cfg(feature = "trace")]
        println!("trace: Create topic: {}", uri);
        println!("Create topic \"{}\" request sending...", name);

        let req = Request::builder()
            .method(Method::PUT)
            .uri(uri)
            .body("".into())?;
        #[cfg(feature = "trace")]
        println!("trace: {:?}", req);    

        let mut resp = self.inner.request(req).await?;

        #[cfg(feature = "trace")]
        println!("trace: {:?}", resp);

        let mut body = BytesMut::new();
        common::recv_resp_body(&mut resp, |chunk| {
            body.put(chunk);
            Ok(())
        }).await?;

        let status = resp.status();
        if !status.is_success() {
            println!("Topic create failure: server says: \"{}\": {:?}", status, body);
            return Err(Box::new(common::Error::Status(status)));
        }

        println!("Topic \"{}\" created", name);
        Ok(())
    }

    pub async fn add_message(&mut self, topic: &str, text: &str, key: &str) -> common::Result<()> {
        let uri = uri!(self, "/message?topic={}", percent_encode(topic));
        #[cfg(feature = "trace")]
        println!("trace: Add message: {}", uri);

        let req = Request::builder()
            .method(Method::POST)
            .uri(uri)
            .header("content-type", "application/json")
            .body(Body::from(format!(
                "{{ \"text\": \"{}\", \"key\": \"{}\" }}", text, key
            )))?;

        println!("Add message to topic \"{}\" request sending...", topic);

        let mut resp = self.inner.request(req).await?;
        #[cfg(feature = "trace")]
        println!("trace: {:?}", resp);

        let mut body = BytesMut::new();
        common::recv_resp_body(&mut resp, |chunk| {
            body.put(chunk);
            Ok(())
        }).await?;

        let status = resp.status();
        if !status.is_success() {
            println!("Add message failure: server says: \"{}\": {:?}", status, body);
        return Err(Box::new(common::Error::Status(status)));
        }

        println!("Message to topic \"{}\" added", topic);

        Ok(())
    }

    pub async fn messages(&mut self) -> common::Result<()> {
        let uri = uri!(self, "/messages?user={}", percent_encode(&self.user));
        #[cfg(feature = "trace")]
        println!("trace: Messages list: {}", uri);
        println!("Get message list request sending...");

        let req = Request::builder()
            .method(Method::GET)
            .uri(uri)
            .body("".into())?;

        #[cfg(feature = "trace")]
        println!("trace: {:?}", req);

        let mut resp = self.inner.request(req).await?;

        #[cfg(feature = "trace")]
        println!("trace: {:?}", resp);

        let mut body = BytesMut::new();
        common::recv_resp_body(&mut resp, |chunk| {
            body.put(chunk);
            Ok(())
        }).await?;

        let status = resp.status();
        if !status.is_success() {
            println!("Get message list failure: server says: \"{}\": {:?}", status, body);
            return Err(Box::new(common::Error::Status(status)));
        }

        println!("Messages for \"{}\" {:?}", self.user, body);
        Ok(())
    }
}
