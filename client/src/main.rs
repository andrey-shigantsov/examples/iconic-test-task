use iconic_broker_client::Client;

#[tokio::main]
pub async fn main() -> common::Result<()> {
    // TODO: get host from cmd arg
    let host = "127.0.0.1:7777";
    
    let user1 = "First User";
    let mut client1 = Client::new(host, user1);

    let user2 = "Second User";
    let mut client2 = Client::new(host, user2);

    let topic1 = "Some topic";
    client1.create_topic(topic1).await.ok();

    let topic2 = "Other topic";
    client1.create_topic(topic2).await.ok();

    client2.topics().await.ok();

    client1.add_message(topic1, "text1", "").await.ok();
    client1.add_message(topic2, "text2", "").await.ok();
    client1.add_message(topic1, "text3", "").await.ok();

    let msgkey1 = "some key";
    client1.add_message(topic1, "with key1 #1", msgkey1).await.ok();
    client1.add_message(topic1, "with key1 #2", msgkey1).await.ok();

    let msgkey2 = "another key";
    client1.add_message(topic1, "with key2 #1", msgkey2).await.ok();
    client1.add_message(topic1, "with key2 #2", msgkey2).await.ok();

    client2.subscribe(topic1).await.ok();
    client2.subscribe(topic2).await.ok();
    client2.messages().await.ok();

    client2.unsubscribe(topic1).await.ok();
    client2.messages().await.ok();

    Ok(())
}